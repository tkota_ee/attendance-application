import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()

export class EmpService {

  constructor(private http: Http) { }
  getEmployees() {
    return this.http.get("http://194.135.83.107:3000/api/employees").pipe(
      map(res => res.json())
    )}

  addEmployee(info) {
    return this.http.post('http://194.135.83.107:3000/api/employees/', info).pipe(
      map(res => res.json())
    )}

  getEmployee(id) {
    return this.http.get('http://194.135.83.107:3000/api/employees/' + id).pipe(
      map(res => res.json())
    )}
  deleteEmployee(id) {
    return this.http.delete('http://194.135.83.107:3000/api/employees/' + id).pipe(
      map(res => res.json())
    )}
  updateEmployees(id, info) {
    return this.http.put('http://194.135.83.107:3000/api/employees/' + id, info).pipe(
      map(res => res.json())
    )}


}












