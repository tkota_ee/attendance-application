
export class Employee {
    name: string;
    position: string;
    department: string;
    start_time: string;
    end_time: string;
    createdDate: Date;
}
